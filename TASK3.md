# Task 3

1. Изучить команду git stash
2. Переключиться на бранч master
    *  ``` 
       git checkout master
        ```
2. Именить TASK2.MD, дописав в него "PS Я буду делать задание 3"
3. Добавить изменения в Staging Area
    *  ``` 
       git add TASK2.md
        ```
4. Вызвать git stash
    *  ``` 
       git stash
        ```
5. Вызвать git stash list и git status
    *  ``` 
       git stash list
       git status
        ```
6. Переключиться на ветку develop
    *  ``` 
       git checkout develop
        ```
7. Убедиться, что файл TASK2.MD не содержит изменений
    *  ``` 
       git status
        ```
8. Добавить файл TASK3.MD с описанием проделанной работы
9.  Закоммитить изменения
    *  ``` 
        git add TASK3.md
        git commit -m "Add task 3"
        ```
10. Переключиться на master
    *  ``` 
       git checkout master
        ```
11. Вернуть stash изменения TASK2.MD назад
    *  ``` 
       git stash pop
        ```
12. Закоммитить изменения
    *  ``` 
       git add TASK2.md
       git commit -m "stash pop"
        ```
13. Смержить develop в master
    *  ``` 
       git merge develop
        ```
14. Дописать в TASK3.MD проделанные шаги
