# Task 2

10. Вызвать команды diff , status
    *  ``` 
       git diff
       git status
        ```
11. Закоммитить изменения, снова проверить статус
    *  ``` 
       git add TASK1.md
       git commit -m "uptade task1”
       git status
        ```
12. Сделать push в удаленный репозиторий
    *  ``` 
       git push -u origin master
        ```
<hr>

1. Шаги 10-12 описать в новом файле TASK2.MD (выше)
2. Отвести бранч develop, в который залить файл TASK2.MD. Локально и удаленно
    *  ``` 
       git branch develop
       git checkout develop
       git add TASK2.md
       git commit -m "add task2"
       git push -u origin develop
        ```

3. Добавить в конец файла TASK1.MD строчку "Задание провалено!:(". Залить изменения локально и удаленно
    *  ``` 
       git add TASK1.md
       git commit -m "add string task1"
       git push -u origin develop
        ```
4. Переключиться назад на основной бранч
    *  ``` 
       git checkout master
        ```
5. Добавить в файл TASK1.MD строчку "Задание успешно выполнено!:)". Залить изменения локально и удаленно
    *  ``` 
       git add TASK1.md
       git commit -m "add success string"
       git push -u origin master
        ```
6. Выполнить мерж из ветки develop в основную верку. Разрешить конфликт в пользу основной ветки
    *  ``` 
       git merge develop
       git commit -a -m "merge"
       git push -u origin master
        ```
7. Посмотреть лог проделанных изменений
       *  ``` 
       git log
        ```
8. Описать проделанные упражнения в файле TASK2.MD
9. Прислать ссылку на репозиторий на проверку

PS Я буду делать задание 3
